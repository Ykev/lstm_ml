import torch
import torch.nn as nn
#activation functions
import torch.nn.functional as F
#Adam (alot like Stochastic Gradient Descent but faster)
from torch.optim import Adam

import lightning as L
#makes life easier with larg datasets
from torch.utils.data import TensorDataset, DataLoader

NUM_OF_OUTPUTS = 1

class LSTM_NN(L.LightningModule):

    def __init__(self):
        super().__init__()

        self.lstm = nn.LSTM(input_size=1, hidden_size=NUM_OF_OUTPUTS)
        self.learning_rate = 0.001

    def forward(self, input):
        input_trans = input.view(len(input), NUM_OF_OUTPUTS)
        #lstm_out = the short memory outs
        lstm_out, temp = self.lstm(input_trans)

        return lstm_out[-1]

    #sets up the method we'll use to optimize
    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.learning_rate)
    #takes batch of training data from dataloader
    #and the index to that batch.
    # returns the loss of that batch
    def training_step(self, batch, batch_idx):
        input_i, label_i = batch
        output_i = self.forward(input_i[0])
        loss = (output_i - label_i)**2
        #lightning creates log file
        self.log("train_loss", loss)
        #company predictions

        return loss